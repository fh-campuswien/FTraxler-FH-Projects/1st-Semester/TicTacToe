﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TicTacToe_WPF
{
    public partial class TicTacToeButton : UserControl
    {
        private TicTacToeStatus status;

        public TicTacToeButton()
        {
            InitializeComponent();
            status = TicTacToeStatus.NULL;
            applyStatus();
        }

        public TicTacToeStatus Status
        {
            get { return status; }
            set { status = value; applyStatus(); }
        }

        public bool IsSet { get; set; }

        private void applyStatus()
        {
            switch (status)
            {
                case TicTacToeStatus.NULL:
                    {
                        CCircle.Visibility = Visibility.Collapsed;
                        CCross.Visibility = Visibility.Collapsed;
                        IsSet = false;
                    }
                    break;
                case TicTacToeStatus.Circle:
                    {
                        CCircle.Visibility = Visibility.Visible;
                        IsSet = true;
                    }
                    break;
                case TicTacToeStatus.Cross:
                    {
                        CCross.Visibility = Visibility.Visible;
                        IsSet = true;
                    }
                    break;
            }
        }
    }
}
