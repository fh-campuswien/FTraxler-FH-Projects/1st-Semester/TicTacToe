﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TicTacToe_WPF
{
    public partial class MainWindow : Window
    {
        private TicTacToeStatus player;
        int moveCounter;
        bool gameOver;

        public MainWindow()
        {
            InitializeComponent();
            player = TicTacToeStatus.NULL;
            init();
        }

        private void init()
        {
            IEnumerable<TicTacToeButton> buttons = GameField.Children.OfType<TicTacToeButton>();
            foreach (TicTacToeButton b in buttons)
            {
                b.Status = TicTacToeStatus.NULL;
            }
            player = (TicTacToeStatus)1;
            moveCounter = 1;
            gameOver = false;
            infoLabel.Content = "Player " + (int)player + " starts";
        }

        private void check()
        {
            int[] pattern = new int[9];
            IEnumerable<TicTacToeButton> buttons = GameField.Children.OfType<TicTacToeButton>();
            foreach (TicTacToeButton b in buttons)
            {
                pattern[Convert.ToInt32(b.Tag)] = (int)b.Status;
            }
            // check algorithm
            if (
                (pattern[0] == pattern[1] && pattern[1] == pattern[2] && pattern[2] != 0) ||
                (pattern[3] == pattern[4] && pattern[4] == pattern[5] && pattern[5] != 0) ||
                (pattern[6] == pattern[7] && pattern[7] == pattern[8] && pattern[8] != 0) ||
                (pattern[0] == pattern[3] && pattern[3] == pattern[6] && pattern[6] != 0) ||
                (pattern[1] == pattern[4] && pattern[4] == pattern[7] && pattern[7] != 0) ||
                (pattern[2] == pattern[5] && pattern[5] == pattern[8] && pattern[8] != 0) ||
                (pattern[0] == pattern[4] && pattern[4] == pattern[8] && pattern[8] != 0) ||
                (pattern[6] == pattern[4] && pattern[4] == pattern[2] && pattern[2] != 0)
                )
            {
                gameOver = true;
                infoLabel.Content = "Player " + (int)player + " wins";
            }
            else if(moveCounter == 9)
            {
                gameOver = true;
                infoLabel.Content = "Draw";
            }
        }

        private void togglePlayer()
        {
            if (!gameOver)
            {
                if ((int)player == 1)
                    player = (TicTacToeStatus)2;
                else if ((int)player == 2)
                    player = (TicTacToeStatus)1;
                moveCounter++;
                infoLabel.Content = "It's your turn Player " + (int)player;
            }
        }

        private void TicTacToeButton_MouseDown(object sender, MouseButtonEventArgs e)
        {
            TicTacToeButton button = sender as TicTacToeButton;
            //
            if ((!button.IsSet) && (!gameOver))
            {
                button.Status = player;
                if (moveCounter > 4)
                    check();
                togglePlayer();
            }
        }

        private void New_Click(object sender, RoutedEventArgs e)
        {
            init();
        }
    }
}
